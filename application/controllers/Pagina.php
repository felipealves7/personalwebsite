<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'MY_Controller.php';

class Pagina extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('pessoa_model');
        $this->load->model('formacao_model');
        $this->load->model('experiencia_model');
        $this->load->model('trabalho_model');
        $this->load->model('contato_model');
    }

	public function index(){

        $this->form_validation->set_message('required', 'O %s é de preenchimento obrigatório.');
        $this->form_validation->set_message('valid_email', 'O %s deve ser válido.');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('message', 'Message', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header');
            $data['pessoa_conteudo'] = $this->pessoa_model->get_conteudo();
            $data['formacao_conteudo'] = $this->formacao_model->get_conteudo();
            $data['experiencia_conteudo'] = $this->experiencia_model->get_conteudo();
            $data['trabalho_conteudo'] = $this->trabalho_model->get_conteudo();
            $this->load->view('paginas/home', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->contato_model->set_contato();
            $data['alert'] = true;
            $this->load->view('common/header');
            $data['pessoa_conteudo'] = $this->pessoa_model->get_conteudo();
            $data['formacao_conteudo'] = $this->formacao_model->get_conteudo();
            $data['experiencia_conteudo'] = $this->experiencia_model->get_conteudo();
            $data['trabalho_conteudo'] = $this->trabalho_model->get_conteudo();
            $this->load->view('paginas/home', $data);
            $this->load->view('common/footer');
        }
	}   

    public function login(){
        $this->form_validation->set_message('required', 'O %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('login', 'Login', 'required');
        $this->form_validation->set_rules('senha', 'Senha', 'required');
        
        if($this->form_validation->run() === FALSE){
            $this->load->view('common/header_adm');
            $this->load->view('paginas/login');
            $this->load->view('common/footer');
        }
        else{
            if($this->pessoa_model->login()){
                $sessao = array('login'=>$login['login']);
                $this->session->set_userdata($sessao);
                redirect(base_url('administrador/home'));
            }
            else{
                $this->load->view('common/header_adm');
                $this->load->view('paginas/login');
                $this->load->view('common/footer');
            }
        }
    }

    public function administrador(){

        $this->form_validation->set_message('required', 'O %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required');
        $this->form_validation->set_rules('dt_ncto', 'Data de Nascimento', 'required');
        $this->form_validation->set_rules('local_ncto', 'Local de Nascimento', 'required');
        $this->form_validation->set_rules('local_residencia', 'Local Residencia', 'required');
        $this->form_validation->set_rules('profissao', 'Profissão', 'required');
        $this->form_validation->set_rules('dt_ini_profissao', 'Data da Profissão', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $data['pessoa_conteudo'] = $this->pessoa_model->get_pessoa();
            $this->load->view('paginas/administrador', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->pessoa_model->update_pessoa();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $data['pessoa_conteudo'] = $this->pessoa_model->get_pessoa();
            $this->load->view('paginas/administrador', $data);
            $this->load->view('common/footer');
        }
    } 

    public function contato(){
        $this->load->view('common/header_administrador');
        $data['conteudo'] = $this->contato_model->get_contato();
        $this->load->view('paginas/contato', $data);
        $this->load->view('common/footer');
    } 
    public function excluirContato(){
        $this->contato_model->excluirContato();
        redirect(base_url('administrador/contato'));
    }

    public function portifolio(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('titulo', 'Nome', 'required');
        $this->form_validation->set_rules('dt_trab', 'Data Trabalho', 'required');
        $this->form_validation->set_rules('descr', 'Descrição', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->trabalho_model->get_portifolio();
            $this->load->view('paginas/portifolio', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->trabalho_model->update_Portifolio();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->trabalho_model->get_portifolio();
            $this->load->view('paginas/portifolio', $data);
            $this->load->view('common/footer');
        }
    } 
    public function excluirPortifolio(){
        $this->trabalho_model->excluirPortifolio();
        redirect(base_url('administrador/portifolio'));
    }
    public function cadastrarPortifolio(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('titulo', 'Nome', 'required');
        $this->form_validation->set_rules('dt_trab', 'Data Trabalho', 'required');
        $this->form_validation->set_rules('descr', 'Descrição', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarPortifolio', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->trabalho_model->set_Portifolio();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarPortifolio', $data);
            $this->load->view('common/footer');
        }
    } 

    public function formacao(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('instituicao', 'Instituição', 'required');
        $this->form_validation->set_rules('formacao', 'Formação', 'required');
        $this->form_validation->set_rules('dt_formacao', 'Data de Formação', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->formacao_model->get_formacao();
            $this->load->view('paginas/formacao', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->formacao_model->update_Formacao();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->formacao_model->get_formacao();
            $this->load->view('paginas/formacao', $data);
            $this->load->view('common/footer');
        }
    } 
    public function excluirFormacao(){
        $this->formacao_model->excluirFormacao();
        redirect(base_url('administrador/formacao'));
    }
    public function cadastrarFormacao(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('instituicao', 'Instituição', 'required');
        $this->form_validation->set_rules('formacao', 'Formação', 'required');
        $this->form_validation->set_rules('dt_formacao', 'Data de Formação', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarFormacao', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->formacao_model->set_Formacao();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarFormacao', $data);
            $this->load->view('common/footer');
        }
    } 

    public function experiencia(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('titulo', 'Titulo', 'required');
        $this->form_validation->set_rules('descr', 'Descrição', 'required');
        $this->form_validation->set_rules('dt_ini', 'Data de Inicio', 'required');
        $this->form_validation->set_rules('dt_fim', 'Data de Fim', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->experiencia_model->get_experiencia();
            $this->load->view('paginas/experiencia', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->experiencia_model->update_Experiencia();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $data['conteudo'] = $this->experiencia_model->get_experiencia();
            $this->load->view('paginas/experiencia', $data);
            $this->load->view('common/footer');
        }
    } 
    public function excluirExperiencia(){
        $this->experiencia_model->excluirExperiencia();
        redirect(base_url('administrador/experiencia'));
    }
    public function cadastrarExperiencia(){
        $this->form_validation->set_message('required', 'O campo %s é de preenchimento obrigatório.');
        $this->form_validation->set_rules('titulo', 'Titulo', 'required');
        $this->form_validation->set_rules('descr', 'Descrição', 'required');
        $this->form_validation->set_rules('dt_ini', 'Data de Inicio', 'required');
        
        if($this->form_validation->run() === FALSE){
            $data['alert'] = false;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarExperiencia', $data);
            $this->load->view('common/footer');
        }
        else{
            $this->experiencia_model->set_Experiencia();
            $data['alert'] = true;
            $this->load->view('common/header_administrador');
            $this->load->view('paginas/cadastrarExperiencia', $data);
            $this->load->view('common/footer');
        }
    } 

    public function logoff(){
        $this->session->sess_destroy();
        redirect(base_url('administrador'));
    }  

    public function pdf(){
        $data['pessoa_conteudo'] = $this->pessoa_model->get_conteudo();
        $data['formacao_conteudo'] = $this->formacao_model->get_conteudo();
        $data['experiencia_conteudo'] = $this->experiencia_model->get_conteudo();
        $data['trabalho_conteudo'] = $this->trabalho_model->get_conteudo();
         
        //load the view, pass the variable and do not show it but "save" the output into $html variable
        $html=$this->load->view('pdf_output', $data, true); 
         
        //this the the PDF filename that user will get to download
        $pdfFilePath = "pdf.pdf";
         
        //load mPDF library
        $this->load->library('m_pdf');
        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->m_pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD)
        $pdf->Output($pdfFilePath, "D");
    }
        
}