<!DOCTYPE HTML>
<html>
	<head>
		<title>Prologue by HTML5 UP</title>
		<meta charset="utf-8" />
			
		<!-- Bootstrap core CSS -->			
		<link href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>"	rel="stylesheet">
		
		<!-- Bootstrap theme -->
		<link href="<?php echo base_url ('bootstrap/css/bootstrap-theme.min.css');?>"	rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<!--link href="< ?php echo base_url ( 'bootstrap/css/theme.css' );?>" rel="stylesheet"-->
		
		<!-- Custom styles for app specific stuff -->
		<link href="<?php echo base_url ('assets/css/table.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/layout.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/main_nav.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/dashboard.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/tablesorter/blue/style.css');?>" rel="stylesheet">
		
		<script src="<?php echo base_url('assets/js/jquery/jquery.js');?>"></script>
		<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>	
		<script src="<?php echo base_url('assets/js/jquery/jquery.tablesorter.min.js');?>"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link href="<?php echo base_url('assets/css/main.css');?>" rel="stylesheet">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>

		<div class="container">
				<div class="row col-sm-12">
					<div class="col-sm-12" style="padding-top:0px;">
						<h2 class="alt"><strong>Felipe Alves</strong></h2>
					</div>
					<div class="col-sm-12">
						<?php echo $pessoa_conteudo; ?>
					</div>
				</div>
				<div class="row col-sm-12">
					<header>
								<h2>Formação</h2>
							</header>
							<?php echo $formacao_conteudo; ?>
				</div>
				<div class="row col-sm-12">
							<header>
								<h2>Experiencia</h2>
							</header>
							<?php echo $experiencia_conteudo; ?>
				</div>
				<div class="row col-sm-12">
					<header>
								<h2>Portifólio</h2>
							</header>

							<?php echo $trabalho_conteudo; ?>
				</div>
		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.scrollzer.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>