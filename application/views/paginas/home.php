				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<div class="row col-sm-12">
									<div class="col-sm-12">
										<h4 class="alt">Hi! I'm</h4>
									</div>
									<div class="col-sm-12" style="padding-top:0px;">
										<h2 class="alt"><strong>Felipe Alves</strong></h2>
									</div>
									<div class="col-sm-12">
										<?php echo $pessoa_conteudo; ?>
									</div>
								</div>
							</header>

						</div>
					</section>

				<!-- formacao -->
					<section id="formacao" class="two">
						<div class="container">

							<header>
								<h2>Formação</h2>
							</header>
							<?php echo $formacao_conteudo; ?>
							<header>
								<h2>Experiencia</h2>
							</header>
							<?php echo $experiencia_conteudo; ?>

						</div>
					</section>

				<!-- portifolio -->
					<section id="portifolio" class="three">
						<div class="container">

							<header>
								<h2>Portfólio</h2>
							</header>

							<?php echo $trabalho_conteudo; ?>

						</div>
					</section>

				<!-- contato -->
					<section id="contato" class="four">
						<div class="container">

							<header>
								<h2>Contato</h2>
							</header>
							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo validation_errors('<p style="color:red; margin-bottom:0px;">', '</p>');
										if($alert === true){
											echo '<p style="margin-bottom:0px;">Contato enviado com Sucesso!</p>';
										}
									?>
								</div>
							</div>
							<form method="post" action=<?php echo base_url('#contato'); ?>>
								<div class="row">
									<div class="6u 12u$(mobile)"><input type="text" name="name" placeholder="Name" /></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="email" placeholder="Email" /></div>
									<div class="12u$">
										<textarea name="message" placeholder="Message"></textarea>
									</div>
									<div class="12u$">
										<input type="submit" value="Enviar mensagem" />
									</div>
								</div>
							</form>
							<br><br>
							<div class="fb-comments" data-href="<?php echo base_url(uri_string()); ?>" data-width="100%" data-numposts="5"></div>

						</div>
					</section>