
				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2>Homepage - Sobre</h2>
							</header>
							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo validation_errors('<p style="color:red; margin-bottom:0px;">', '</p>');
										if($alert === true){
											echo '<p style="margin-bottom:0px;">Homepage - Sobre alterada com Sucesso!</p>';
										}
									?>
								</div>
							</div>
							<form method="post" action=<?php echo base_url('administrador/home'); ?>>
								<div class="row">
									<div class="6u 12u$(mobile)"><input type="text" name="nome" placeholder="Nome" value="<?php echo $pessoa_conteudo['nome'];?>"/></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="sobrenome" placeholder="Sobrenome" value="<?php echo $pessoa_conteudo['sobrenome'];?>"/></div>
									<div class="6u 12u$(mobile)"><input type="text" name="dt_ncto" placeholder="Data de Nascimento" value="<?php echo $pessoa_conteudo['dt_ncto'];?>"/></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="local_ncto" placeholder="Local de Nascimento" value="<?php echo $pessoa_conteudo['local_ncto'];?>"/></div>
									<div class="6u 12u$(mobile)"><input type="text" name="local_residencia" placeholder="Local Residencia" value="<?php echo $pessoa_conteudo['local_residencia'];?>"/></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="profissao" placeholder="Profissão" value="<?php echo $pessoa_conteudo['profissao'];?>"/></div>
									<div class="6u 12u$(mobile)"><input type="text" name="dt_ini_profissao" placeholder="Data da Profissão" value="<?php echo $pessoa_conteudo['dt_ini_profissao'];?>"/></div>
									<div class="12u$">
										<input type="submit" value="Alterar" />
									</div>
								</div>
							</form>

						</div>
					</section>