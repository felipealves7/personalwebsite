				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2>Login</h2>
							</header>
							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo validation_errors('<p style="color:white; margin-bottom:0px;">', '</p>');
									?>
								</div>
							</div>
							<form method="post" action=<?php echo base_url('administrador'); ?>>
								<div class="row">
									<div class="12u 12u$(mobile)"><input type="text" name="login" placeholder="Login" /></div>
									<div class="12u$ 12u$(mobile)"><input type="password" name="senha" placeholder="Senha" /></div>
									<div class="12u$">
										<input type="submit" value="Entrar" />
									</div>
								</div>
							</form>

						</div>
					</section>