
				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2>Cadastro de Formação</h2>
							</header>
							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo validation_errors('<p style="color:red; margin-bottom:0px;">', '</p>');
										if($alert === true){
											echo '<p style="margin-bottom:0px;">Formação cadastrado com Sucesso!</p>';
										}
									?>
								</div>
							</div>
							<form method="post" action="<?php echo base_url("administrador/cadastrarFormacao");?>">
								<div class="row">
									<div class="6u 12u$(mobile)"><input type="text" name="instituicao" placeholder="Instituição"/></div>
									<div class="6u$ 12u$(mobile)"><input type="text" name="formacao" placeholder="Formacão"/></div>
									<div class="6u 12u$(mobile)"><input type="text" name="dt_formacao" placeholder="Data de Formação 0000-00-00"/></div>
									<div class="12u$">
										<textarea name="descr" placeholder="Descrição"></textarea>
									</div>
									<div class="12u$">
										<input type="submit" value="Cadastrar" />
									</div><br><br>
								</div>
							</form>

						</div>
					</section>