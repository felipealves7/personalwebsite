<!DOCTYPE HTML>
<html>
	<head>
		<title>Felipe Alves</title>
		<meta charset="utf-8" />
			
		<!-- Bootstrap core CSS -->			
		<link href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>"	rel="stylesheet">
		
		<!-- Bootstrap theme -->
		<link href="<?php echo base_url ('bootstrap/css/bootstrap-theme.min.css');?>"	rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<!--link href="< ?php echo base_url ( 'bootstrap/css/theme.css' );?>" rel="stylesheet"-->
		
		<!-- Custom styles for app specific stuff -->
		<link href="<?php echo base_url ('assets/css/table.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/layout.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/main_nav.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/dashboard.css');?>" rel="stylesheet">
		<link href="<?php echo base_url ('assets/css/tablesorter/blue/style.css');?>" rel="stylesheet">
		
		<script src="<?php echo base_url('assets/js/jquery/jquery.js');?>"></script>
		<script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>	
		<script src="<?php echo base_url('assets/js/jquery/jquery.tablesorter.min.js');?>"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link href="<?php echo base_url('assets/css/main.css');?>" rel="stylesheet">
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  	if (d.getElementById(id)) return;
			  	js = d.createElement(s); js.id = id;
			  	js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6";
			  	fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

		
		<!-- Header -->
			<div id="header">

				<div class="top">

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="<?php echo base_url('assets/img/avatar.jpg');?>" alt="" /></span>
							<h1 id="title">Felipe Alves</h1>
							<p>Tech Lover</p>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Homepage - Sobre</span></a></li>
								<li><a href="#formacao" id="portfolio-link" class="skel-layers-ignoreHref"><span class="icon fa-th">Formação</span></a></li>
								<li><a href="#portifolio" id="about-link" class="skel-layers-ignoreHref"><span class="icon fa-user">Portifólio</span></a></li>
								<li><a href="#contato" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-envelope">Contato</span></a></li>
								<li><a href="<?php echo base_url('PDF'); ?>" id="contact-link" class="skel-layers-ignoreHref"><span class="icon fa-envelope">Baixar PDF</span></a></li>
							</ul>
						</nav>

				</div>

				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="http://facebook.com/felipealves7" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="http://br.linkedin.com/in/felipealves7" class="icon fa-linkedin"><span class="label">Dribbble</span></a></li>
							<li><a href="mailto:felipe.s.alves7@gmail.com" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>

				</div>

			</div>

		<!-- Main -->
			<div id="main">