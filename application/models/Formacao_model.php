<?php
	class Formacao_model extends CI_Model{
		public function __construct(){
			$this->load->database();
	        $this->load->model('dao/FormacaoDAO');
		}

		public function get_conteudo(){
			$formacao = $this->FormacaoDAO->get_formacao();
			$conteudo = "";
			foreach($formacao as $value){
				$conteudo = $conteudo.'<p><strong>Instituição:</strong> '.$value["instituicao"];
				$conteudo = $conteudo.'<br><strong>Formação:</strong> '.$value["formacao"];
				$conteudo = $conteudo.'<br><strong>Data de formação:</strong> '.date('d/m/Y', strtotime($value["dt_formacao"]));
				if($value["descr"] != "" && $value["descr"] != null){
					$conteudo = $conteudo.'<br><strong>Descrição:</strong> <br>'.str_replace("\r\n","<br>",$value["descr"]);
				}
				$conteudo = $conteudo.'</p>';
			}
			return $conteudo;
		}

		public function get_formacao(){
			$contato = $this->FormacaoDAO->get_Formacao();
			$conteudo = '<a href="'.base_url("administrador/cadastrarFormacao").'">
							<div class="12u$">
									<input type="submit" value="Novo" /><br><br><br>
							</div>
						</a>';
			foreach($contato as $cont){
				$conteudo = $conteudo.'<form method="post" action="'.base_url("administrador/formacao").'">
											<div class="row">
												<div class="6u 12u$(mobile)"><input type="text" name="instituicao" placeholder="Instituição" value="'.$cont["instituicao"].'"/></div>
												<div class="6u$ 12u$(mobile)"><input type="text" name="formacao" placeholder="Formacão" value="'.$cont["formacao"].'"/></div>
												<div class="6u 12u$(mobile)"><input type="text" name="dt_formacao" placeholder="Data de Formação 0000-00-00" value="'.$cont["dt_formacao"].'"/></div>
												<div class="12u$">
													<textarea name="descr" placeholder="Descrição">'.$cont["descr"].'</textarea>
												</div>
												<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
												<div class="12u$">
													<input type="submit" value="Alterar" />
												</div><br><br>
											</div>
										</form>
										<form method="post" action="'.base_url("administrador/excluirFormacao").'">
											<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
											<div class="12u$">
													<input type="submit" value="Excluir" /><br><br><br>
												</div>
										</form>';
			}
			return $conteudo;
		}

		public function excluirFormacao(){
	        $id = $this->input->post('id');
			$this->FormacaoDAO->delete_Formacao($id);
		}

		public function update_Formacao(){
			$id = $this->input->post('id');
			$instituicao = $this->input->post('instituicao');
			$formacao = $this->input->post('formacao');
			$dt_formacao = $this->input->post('dt_formacao');
			$descr = $this->input->post('descr');
			$this->FormacaoDAO->update_Formacao($id, $instituicao, $formacao, $dt_formacao, $descr);
		}

		public function set_Formacao(){
			$instituicao = $this->input->post('instituicao');
			$formacao = $this->input->post('formacao');
			$dt_formacao = $this->input->post('dt_formacao');
			$descr = $this->input->post('descr');
			$this->FormacaoDAO->set_Formacao($instituicao, $formacao, $dt_formacao, $descr);
		}
	}
?>