<?php
	class Trabalho_model extends CI_Model{
		public function __construct(){
			$this->load->database();
	        $this->load->model('dao/TrabalhoDAO');
		}

		public function get_conteudo(){
			$trabalho = $this->TrabalhoDAO->get_trabalho();
			$conteudo = "";
			foreach($trabalho as $value){
				$conteudo = $conteudo.'<p><strong>Titulo:</strong> '.$value["titulo"];
				$conteudo = $conteudo.'<br><strong>Data:</strong> '.date('d/m/Y', strtotime($value["dt_trab"]));
				$conteudo = $conteudo.'<br><strong>Descrição:</strong> <br>'.str_replace("\r\n","<br>",$value["descr"]);
				if($value["comentario"] != "" && $value["comentario"] != null){
					$conteudo = $conteudo.'<br><strong>Comentário:</strong> <br>'.str_replace("\r\n","<br>",$value["comentario"]);
				}
				$conteudo = $conteudo.'<br><strong>Tipo:</strong> '.$value["tipo"];
				$conteudo = $conteudo.'</p>';
			}
			return $conteudo;
		}

		public function get_portifolio(){
			$contato = $this->TrabalhoDAO->get_Trabalho();
			$conteudo = '<a href="'.base_url("administrador/cadastrarFormacao").'">
							<div class="12u$">
									<input type="submit" value="Novo" /><br><br><br>
							</div>
						</a>';
			foreach($contato as $cont){
				$conteudo = $conteudo.'<form method="post" action="'.base_url("administrador/portifolio").'">
											<div class="row">
												<div class="6u 12u$(mobile)"><input type="text" name="titulo" placeholder="Titulo" value="'.$cont["titulo"].'"/></div>
												<div class="6u$ 12u$(mobile)"><input type="text" name="dt_trab" placeholder="Data Trabalho" value="'.$cont["dt_trab"].'"/></div>
												<div class="12u$">
													<textarea name="descr" placeholder="Descrição">'.$cont["descr"].'</textarea>
												</div>
												<div class="12u$">
													<textarea name="comentario" placeholder="Comentário">'.$cont["comentario"].'</textarea>
												</div>
												<div class="6u 12u$(mobile)"><input type="text" name="tipo" placeholder="Tipo" value="'.$cont["tipo"].'"/></div>
												<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
												<div class="12u$">
													<input type="submit" value="Alterar" />
												</div><br><br>
											</div>
										</form>
										<form method="post" action="'.base_url("administrador/excluirPortifolio").'">
											<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
											<div class="12u$">
													<input type="submit" value="Excluir" /><br><br><br>
												</div>
										</form>';
			}
			return $conteudo;
		}

		public function excluirPortifolio(){
	        $id = $this->input->post('id');
			$this->TrabalhoDAO->delete_Trabalho($id);
		}

		public function update_Portifolio(){
			$id = $this->input->post('id');
			$titulo = $this->input->post('titulo');
			$dt_trab = $this->input->post('dt_trab');
			$descr = $this->input->post('descr');
			$comentario = $this->input->post('comentario');
			$tipo = $this->input->post('tipo');
			$this->TrabalhoDAO->update_Trabalho($id, $titulo, $dt_trab, $descr, $comentario, $tipo);
		}

		public function set_Portifolio(){
			$titulo = $this->input->post('titulo');
			$dt_trab = $this->input->post('dt_trab');
			$descr = $this->input->post('descr');
			$comentario = $this->input->post('comentario');
			$tipo = $this->input->post('tipo');
			$this->TrabalhoDAO->set_Trabalho($titulo, $dt_trab, $descr, $comentario, $tipo);
		}
	}
?>