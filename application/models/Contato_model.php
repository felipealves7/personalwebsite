<?php
	class Contato_model extends CI_Model{
		public function __construct(){
			$this->load->database();
	        $this->load->model('dao/ContatoDAO');
		}

		public function set_contato(){
	        $titulo = $this->input->post('name');
	        $remetente = $this->input->post('email');
	        $conteudo = $this->input->post('message');
	        date_default_timezone_set('America/Sao_Paulo');
	        $dt_envio = date('Y-m-d H:i:s');
			$this->ContatoDAO->set_Contato($titulo,$remetente,$conteudo,$dt_envio);
		}

		public function get_contato(){
			$contato = $this->ContatoDAO->get_contato();
			$conteudo = "";
			foreach($contato as $cont){
				$conteudo = $conteudo.'<form method="post" action="'.base_url("administrador/excluirContato").'">
											<div class="row">
												<div class="12u 12u$(mobile)">
													<p style="margin-bottom:0px;">'.$cont["dt_envio"].'</p>
												</div>
												<div class="6u 12u$(mobile)"><input type="text" name="name" placeholder="Name" value="'.$cont["titulo"].'"/></div>
												<div class="6u$ 12u$(mobile)"><input type="text" name="email" placeholder="Email" value="'.$cont["remetente"].'"/></div>
												<div class="12u$">
													<textarea name="message" placeholder="Message">'.$cont["conteudo"].'</textarea>
												</div>
												<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
												<div class="12u$">
													<input type="submit" value="Excluir" /><br><br>
												</div>
											</div>
										</form>';
			}
			return $conteudo;
		}

		public function excluirContato(){
	        $id = $this->input->post('id');
			$this->ContatoDAO->delete_Contato($id);
		}
	}
?>