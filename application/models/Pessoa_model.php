<?php
	class Pessoa_model extends CI_Model{
		public function __construct(){
			$this->load->database();
	        $this->load->model('dao/PessoaDAO');
		}

		public function get_conteudo(){
			$pessoa = $this->PessoaDAO->get_pessoa();
			$conteudo = '<p>Meu nome é '.$pessoa["nome"].' '.$pessoa["sobrenome"].', nasci em '.date('d/m/Y', strtotime($pessoa["dt_ncto"])).'. Sou natural de '.$pessoa["local_ncto"].', mas atualmente moro em '.$pessoa["local_residencia"].'. Trabalho com '.$pessoa["profissao"].' desde '.date('d/m/Y', strtotime($pessoa["dt_ini_profissao"])).'.</p>';
			return $conteudo;
		}

		public function get_pessoa(){
			return $this->PessoaDAO->get_pessoa();
		}

		public function update_pessoa(){
			$nome = $this->input->post('nome');
			$sobrenome = $this->input->post('sobrenome');
			$dt_ncto = $this->input->post('dt_ncto');
			$local_ncto = $this->input->post('local_ncto');
			$local_residencia = $this->input->post('local_residencia');
			$profissao = $this->input->post('profissao');
			$dt_ini_profissao = $this->input->post('dt_ini_profissao');
			$this->PessoaDAO->update_pessoa($nome, $sobrenome, $dt_ncto, $local_ncto, $local_residencia, $profissao, $dt_ini_profissao);
		}

		public function login(){
	        $login = $this->input->post('login');
	        $senha = $this->input->post('senha');
	        $pessoa = $this->PessoaDAO->get_pessoa();
	        if($login == $pessoa['login'] && $senha == $pessoa['senha']){
	        	return true;
	        }
	        return false;
		}
	}
?>