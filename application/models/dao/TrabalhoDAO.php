<?php

class TrabalhoDAO extends CI_Model {
    function TrabalhoDAO() {
        parent::__construct();
        $this->load->database();
    }

    function get_Trabalho(){
        $this->db->select('*');
        $this->db->from('trabalho');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function set_Trabalho($titulo, $dt_trab, $descr, $comentario, $tipo){
        $trabalho = array(
            'id_pessoa' => '1',
            'titulo' => $titulo,
            'dt_trab' => $dt_trab,
            'descr' => $descr,
            'comentario' => $comentario,
            'tipo' => $tipo
            );
        $this->db->insert('trabalho', $trabalho);
    }

    function update_Trabalho($id, $titulo, $dt_trab, $descr, $comentario, $tipo){
        $trabalho = array(
            'id' => $id,
            'titulo' => $titulo,
            'dt_trab' => $dt_trab,
            'descr' => $descr,
            'comentario' => $comentario,
            'tipo' => $tipo
            );
        $this->db->where('id', $id);
        $this->db->update('trabalho', $trabalho);
    }

    function delete_Trabalho($id){
        $this->db->where('id',$id);
        $this->db->delete('trabalho');
    }

}
