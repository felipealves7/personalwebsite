<?php

class ExperienciaDAO extends CI_Model {
    function ExperienciaDAO() {
        parent::__construct();
        $this->load->database();
    }

    function get_Experiencia(){
        $this->db->select('*');
        $this->db->from('experiencia');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function set_Experiencia($titulo, $descr, $dt_ini, $dt_fim){
        $experiencia = array(
            'id_pessoa' => '1',
            'titulo' => $titulo,
            'descr' => $descr,
            'dt_ini' => $dt_ini,
            'dt_fim' => $dt_fim
            );
        $this->db->insert('experiencia', $experiencia);
    }

    function update_Experiencia($id, $titulo, $descr, $dt_ini, $dt_fim){
        $experiencia = array(
            'titulo' => $titulo,
            'descr' => $descr,
            'dt_ini' => $dt_ini,
            'dt_fim' => $dt_fim
            );
        $this->db->where('id', $id);
        $this->db->update('experiencia', $experiencia);
    }

    function delete_Experiencia($id){
        $this->db->where('id',$id);
        $this->db->delete('experiencia');
    }

}
