<?php

class FormacaoDAO extends CI_Model {
    function FormacaoDAO() {
        parent::__construct();
        $this->load->database();
    }

    function get_Formacao(){
        $this->db->select('*');
        $this->db->from('formacao');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function set_Formacao($instituicao, $formacao, $dt_formacao, $descr){
        $formacao = array(
            'id_pessoa' => '1',
            'instituicao' => $instituicao,
            'formacao' => $formacao,
            'dt_formacao' => $dt_formacao,
            'descr' => $descr
            );
        $this->db->insert('formacao', $formacao);
    }

    function update_Formacao($id, $instituicao, $formacao, $dt_formacao, $descr){
        $formacao = array(
            'instituicao' => $instituicao,
            'formacao' => $formacao,
            'dt_formacao' => $dt_formacao,
            'descr' => $descr
            );
        $this->db->where('id', $id);
        $this->db->update('formacao', $formacao);
    }

    function delete_Formacao($id){
        $this->db->where('id',$id);
        $this->db->delete('formacao');
    }

}
