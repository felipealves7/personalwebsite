<?php

class ContatoDAO extends CI_Model {
    function ContatoDAO() {
        parent::__construct();
        $this->load->database();
    }

    function get_Contato(){
        $this->db->select('*');
        $this->db->from('msgs_contato');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function set_Contato($titulo,$remetente,$conteudo,$dt_envio){
        $contato = array(
            'titulo' => $titulo,
            'remetente' => $remetente,
            'conteudo' => $conteudo,
            'dt_envio' => $dt_envio
            );
        $this->db->insert('msgs_contato', $contato);
    }

    function delete_Contato($id){
        $this->db->where('id',$id);
        $this->db->delete('msgs_contato');
    }

}
