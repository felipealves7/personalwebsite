<?php

class PessoaDAO extends CI_Model {
    function PessoaDAO() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function get_pessoa(){
        $this->db->select('*');
        $this->db->from('pessoa');
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_pessoa($nome, $sobrenome, $dt_ncto, $local_ncto, $local_residencia, $profissao, $dt_ini_profissao){
        $pessoa = array(
            'nome' => $nome,
            'sobrenome' => $sobrenome,
            'dt_ncto' => $dt_ncto,
            'local_ncto' => $local_ncto,
            'local_residencia' => $local_residencia,
            'profissao' => $profissao,
            'dt_ini_profissao' => $dt_ini_profissao
            );
        $this->db->update('pessoa', $pessoa);
    }

}
