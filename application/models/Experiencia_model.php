<?php
	class Experiencia_model extends CI_Model{
		public function __construct(){
			$this->load->database();
	        $this->load->model('dao/ExperienciaDAO');
		}

		public function get_conteudo(){
			$experiencia = $this->ExperienciaDAO->get_experiencia();
			$conteudo = "";
			foreach($experiencia as $value){
				$conteudo = $conteudo.'<p><strong>Titulo:</strong> '.$value["titulo"];
				$conteudo = $conteudo.'<br><strong>Descrição:</strong>  <br>'.str_replace("\r\n","<br>",$value["descr"]);
				$conteudo = $conteudo.'<br><strong>Data de inicio:</strong> '.date('d/m/Y', strtotime($value["dt_ini"]));
				$conteudo = $conteudo.'<br><strong>Data de término:</strong> '.date('d/m/Y', strtotime($value["dt_fim"]));
				$conteudo = $conteudo.'</p>';
			}
			return $conteudo;
		}

		public function get_experiencia(){
			$contato = $this->ExperienciaDAO->get_Experiencia();
			$conteudo = '<a href="'.base_url("administrador/cadastrarExperiencia").'">
							<div class="12u$">
									<input type="submit" value="Novo" /><br><br><br>
							</div>
						</a>';
			foreach($contato as $cont){
				$conteudo = $conteudo.'<form method="post" action="'.base_url("administrador/experiencia").'">
											<div class="row">
												<div class="6u 12u$(mobile)"><input type="text" name="titulo" placeholder="Titulo" value="'.$cont["titulo"].'"/></div>
												<div class="12u$">
													<textarea name="descr" placeholder="Descrição">'.$cont["descr"].'</textarea>
												</div>
												<div class="6u 12u$(mobile)"><input type="text" name="dt_ini" placeholder="Data inicio 0000-00-00" value="'.$cont["dt_ini"].'"/></div>
												<div class="6u$ 12u$(mobile)"><input type="text" name="dt_fim" placeholder="Data Fim 0000-00-00" value="'.$cont["dt_fim"].'"/></div>
												<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
												<div class="12u$">
													<input type="submit" value="Alterar" />
												</div><br><br>
											</div>
										</form>
										<form method="post" action="'.base_url("administrador/excluirExperiencia").'">
											<input type="text" name="id" class="hidden" value="'.$cont["id"].'">
											<div class="12u$">
													<input type="submit" value="Excluir" /><br><br><br>
												</div>
										</form>';
			}
			return $conteudo;
		}

		public function excluirExperiencia(){
	        $id = $this->input->post('id');
			$this->ExperienciaDAO->delete_Experiencia($id);
		}

		public function update_Experiencia(){
			$id = $this->input->post('id');
			$titulo = $this->input->post('titulo');
			$descr = $this->input->post('descr');
			$dt_ini = $this->input->post('dt_ini');
			$dt_fim = $this->input->post('dt_fim');
			$this->ExperienciaDAO->update_Experiencia($id, $titulo, $descr, $dt_ini, $dt_fim);
		}

		public function set_Experiencia(){
			$id = $this->input->post('id');
			$titulo = $this->input->post('titulo');
			$descr = $this->input->post('descr');
			$dt_ini = $this->input->post('dt_ini');
			$dt_fim = $this->input->post('dt_fim');
			$this->ExperienciaDAO->set_Experiencia($titulo, $descr, $dt_ini, $dt_fim);
		}
	}
?>